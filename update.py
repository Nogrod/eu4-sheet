import os

import requests
import zipfile
import io

import app


def main():
    """
    Check if a new version is available download and install it
    :return:
    """
    # get the name from the latest wheel
    response = requests.get('https://gitlab.com/api/v4/projects/16573384/repository/tags?search=wheel&per_page=1')
    tag_name = response.json()[0].get('name')

    version = tag_name.split("-")[1]

    if version == app.__version__:
        print("Program is up-to-date")
        return

    folder_name = f"eu4-sheet-{tag_name}"

    file = download_zip_file(folder_name, tag_name)

    try:
        # unzip file
        with zipfile.ZipFile(file, 'r') as zip_ref:
            zip_ref.extractall(".")

        update_wheel(folder_name, version)
        update_base_config_path(folder_name)
        # update package
    except Exception:
        print("Update FAILED")


def download_zip_file(folder_name: str, tag_name: str):
    # download and save zip file
    print("Download new Version")
    response = requests.get(f"https://gitlab.com/Nogrod/eu4-sheet/-/archive/{tag_name}/{folder_name}.zip")
    print("New version downloaded")

    # make it to a file like object
    return io.BytesIO(response.content)


def update_wheel(folder_name: str, version: str):
    wheel_path = os.path.join(folder_name, f"eu4_sheet-{version.replace('v', '')}-py3-none-any.whl")
    pip_path = os.path.join("env", "Scripts", "pip.exe")

    os.system(f"{pip_path} install {wheel_path}")


def update_base_config_path(folder_name: str):
    # update base_config_data.yml
    if os.path.isfile("base_config_data.yml"):
        os.remove("base_config_data.yml")

    base_config_path = os.path.join(folder_name, "base_config_data.yml")
    os.rename(base_config_path, "base_config_data.yml")


if __name__ == "__main__":
    main()
