This file explains how illustrative the approximation works, using an example.

Let's assume that we have a percentage of 20% and an exact value of 100.
First a range is calculated as follows:

range value = value * percentage = 100 * 0.2 = 20

![init range](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/init-range.png)

The turquoise line indicates the maximum and minimum value of a range.

This results in the range in which the actual value is located: 100 +- 20 = (80 to 120)
Since the exact value is always in the middle (and this is not our goal), we move the range by a random value but in a way that the exact value is still in the result. This shift is determined randomly.

Depending on how widely you shift, our minimum value for the range can now be between 60 and 100 and our maximum value between 100 and 140, but the distance between minimum and maximum value is always 2 * 20 = 40.

![ranges](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/ranges.png)

The turquoise line indicates the maximum and minimum value of a range. Several example ranges are illustrated here

This determines the initial approximate range, that is, when the program starts. After an update the shift is adjusted (shifted). How strong this shift is depends on the value of the "standard_deviation". This shift is determined with the help of the [Normal_distribution](https://en.wikipedia.org/wiki/Normal_distribution). Care is taken that the exact value is always between the minum and the maximum.
