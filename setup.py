# This is purely the result of trial and error.

from setuptools import setup, find_packages

import app

install_requires = [
    'gspread>=3.6.0',
    'oauth2client>=4.1.3',
    'PyYAML>=5.3.1',
]

# Conditional dependencies:

setup(
    name='eu4_sheet',
    version=app.__version__,
    download_url=f'https://gitlab.com/Nogrod/eu4-sheet/-/archive/v2.2.6/eu4-sheet-{app.__version__}.zip',
    author=app.__author__,
    author_email='gotdp6zto@relay.firefox.com',
    license=app.__licence__,
    packages=find_packages(),
    scripts=['app.py', 'update.py'],
    python_requires='>=3.6',
    install_requires=install_requires,
    project_urls={
        'Source': 'https://gitlab.com/Nogrod/eu4-sheet',
    },
)
