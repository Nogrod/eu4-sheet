Das Programm bietet externe Statistiken für das Spiel Europa Universalis 4, in form von Google Tabellen, die nach Wunsch angepasst werden können und automatisch geupdated werden. So können Daten auch nur ungefähr angegeben werden, also dass nur ein Bereich angegeben werden wird, in dem der wahre Wert liegt (z.B. 89-105 mit einem echten Wert bei 100).
Hierfür liest das Programm ein angegebenes Savegame (Speicherstand) von EU4 aus und lädt anschließend die Daten in mindestens eine Tabelle hoch.
[Hier](https://docs.google.com/spreadsheets/d/1SHBolh6vFlKhixYmWnT-Q5Y3uO8xQPbIJpNCMECiohM/edit?usp=sharing) und [hier](https://docs.google.com/spreadsheets/d/1V3zpvg_MFfk13p8OONNUZvBsvOFjRCuRsHo8f_bH0GE/edit?usp=sharing) kannst du zwei Beispiel Tabellen ansehen.

[Gleiches Dokument ohne Bilder](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/LIESMICH.md)

Informationen wie die Approximation von Daten funktioniert findest du [hier](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/Approximation.md)

Im Folgenden wird erklärt wie Sie das Programm installieren, konfigurieren und verwenden können:

## 0. Download und Installation
- Unter folgendem Link "Source Code (zip)" downloaden
    - https://gitlab.com/Nogrod/eu4-sheet/-/releases
- Nach dem Download kann das Programm in einem beliebigen Ordner mithilfe von [7-zip](https://www.7-zip.org/) oder [winrar](https://www.winrar.de/) entpackt werden. (Installation abgeschlossen)
 
## 1. Anmeldedaten
 
Damit das Programm Google Tabellen die dir gehören verändern kann müssen zunächst folgende Schritte durchgeführt werden.
(ein Google account wird benötigt)
 
### 1.1  Es muss ein neues Projekt erstellt werden
- Hierfür wird ein "Projekt" benötigt (unter der Webseite https://console.developers.google.com), dafür kannst du entweder (falls vorhanden) ein bestehendes verwenden oder ein neues Projekt erstellen. Unter der Seite  musst du ein neues Projekt erstellen. 
- Zum Projekt erstellen gehe unter https://console.developers.google.com und klicke auf "**Projekt auswählen**" -> "**NEUES PROJEKT**"
- Gebe deinem Projekt einen Namen und erstelle das Projekt. Der Speicherort ist irrelevant. 
 
![Create Picture](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/projekt_ausw%C3%A4hlen.png)
 
### 1.2  Füge die Bibliothek "Google Drive API" und "Google Sheets API" deinem Projekt zu
- Damit das Programm die Tabellen bearbeiten kann müssen dem Projekt die zwei oben genannten Bibliotheken hinzugefügt werden.
- Stelle sicher, dass das zuvor erstellte Projekt ausgewählt ist. Oben links neben "**GoogleAPIs**" wird das aktuell ausgewählte Projekt angezeigt.
- Um eine Bibliothek hinzuzufügen musst du zunächst nach dieser Suchen. Du kannst entweder über "**Bibliothek**" suchen oder über die Suchleiste am oberen Rand. 
- Wähle danach die Bibliothek aus und drücke auf "**Aktivieren**". Suche und aktiviere danach die zweite Bibliothek. 
- Wenn du nun auf "**GoogleAPIs**" (oben links) klickst und auf das Dashboard gehst, müssen unten in einer Tabelle zwei einträge mit den Namen "Google Drive API" und "Google Sheets API" vorhanden sein. Wenn einer oder beide Fehlern musst du diese, wie oben beschrieben, noch Aktivieren.
 
 
![search Bib](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/suche_bib.png)
![activate](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/aktiviere_bib.png)
 
 
### 1.3  Füge neue Anmeldaten hinzu
- Nun müssen wir noch Anmeldedaten hinzufügen, damit das Programm später auf die Tabellen zugreifen darf.
- Gehe hierzu unter "**Anmeldedaten**" => "**Anmeldedaten hinzufügen**" => "**Auswahlhilfe**" 
 
![add Bib](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/erstelle_anmeldedaten_hilfe.png)
 
- Verwende anschließend folgende Konfiguration (siehe folgendes Bild):
    - Verwende als API: "Google sheet api"
    - Verwende als Plattform: "Webserver (z.B. Node.js, Tomcat)"
    - Wähle unter "Auf welche Daten wird zugegriffen? ": "Anwendungsdaten" aus
    - Wähle unter "Möchten Sie diese API mit App Engine oder Compute Engine verwenden? " "Nein, ich verwende Sie nicht" aus
    - Drücke "Welche anmeldedaten brauche ich" 
 
![add Bib](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/anmeldedaten_schritt_1.png)
 
- anschließend müssen wir unser Dienstkonto konfigurieren:
    - Setzte einen Namen für das Dienstkonto und weise ihm die "Bearbeiter" Rolle in dem "Projekt" zu
    - Verwende als Schlüsseltyp "JSON"
    - Klicke auf "**Weiter**" und speichere die Datei. Verschiebe diese Datei in den Ordner "credentials" in dem Programm
- (HINWEIS: die gedownloadete Datei ist mit einem Passwort gleich zu setzen. Halte diese Geheim und verschicke sie NICHT weiter oder Kopiere sie. Das Programm verwendet diese Datei nur um zugriff auf die Tabellen zu erhalten die angegeben sind)
 
![add Bib](https://gitlab.com/Nogrod/eu4-sheet/-/raw/master/doku-pic/anmeldedaten_schritt_2.png)
 
Hinweise:
- die E-Mail dieses Dienstkontos wird an anderer stelle benötigt. Du kannst die E-Mail entweder in der Heruntergeladenen Datei finden oder auf der Webseite https://console.developers.google.com unter Anmeldedaten)
 
- Das Tool verwendet ein Dienst von Google, dieser ist KOSTENLOS. Google stellt hierfür ein Kontingent pro Sekunde zur verfügung, überschreitet das Tool dieses Kontingent, gibt es eine Fehlermeldung und das Programm stürzt ab. Hierbei entstehen KEINE Kosten 
 
- Sobald das Tool einmal verwendet wurde, kann die auslastung angeschaut werden.
https://console.developers.google.com => Dashboard => Google Sheets API  => Kontigent
Unterteilt in Read und Write Requests. Kostenlos steht ein Kontingent von 500 pro Sekunde zur verfügung (100 Pro User)
Daher kann durch das Verwenden von mehreren Usern die auslastung erhöht werden. Hierfür kannst du in dem Projekt weitere Anmeldedaten hinzufügen und in der Konfiguration für das Programm diese anpassen (siehe 3 Konfigurierung)
 
    
## 2. Tabellen erstellen
- Als nächstes werden die Tabellen erstellt in die die Daten hochgeladen werden sollen. Gehe hierfür unter https://drive.google.com
- Dort kannst du (mit Rechtsklick) eine neue Tabellen hinzufügen.
- Gebe der Tabelle einen Namen (oben Links), dieser wird später noch benötigt
- Füge noch das zuvor erstellte Dienstkonto als Bearbeiter hinzu
    - Hierfür gehe unter "**Freigabe**" (oben rechts) und Füge die E-Mail eines Dienstkontos als "**Bearbeiter**" hinzu
- Erstelle noch weitere Tabellen wie gewünscht
    
Hinweis:
 
Ein Dienstkonto kann für mehrere Tabellen verwendet werden.
Es kann auch von [dieser](https://docs.google.com/spreadsheets/d/1tKFt1J4TWxT1GwSaB3TXyVDoWkIk3GAAeV6fNYKZvXk/edit?usp=sharing) Tabelle eine Kopie erstellt werden. Diese Tabelle hat bereits eine Formatierung die genutzt werden kann. Der Name sollte jedoch noch angepasst werden. Das Dienstkonto muss auch noch Freigegeben werden.
 
## 3. Konfigurierung
In dem Ordner "configurations" sind deine Konfigurationen abgespeichert, weitere können dort hinzugefügt werden (verwende hierfür am besten die Vorlage => Kopieren => Einfügen => Anpassen )
Es wird ein Texteditor benötigt um diese zu Bearbeiten. Der standard Texteditor von Windows ist hierfür ausreichend.
 
Welche Daten das Tool alle anzeigen kann, welche es ungefähr anzeigen kann und welche Sprache unterstützt werden, kann [hier](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/base_config_data.yml) nachgelesen werden.
 
Welche Einstellung was genau bewirkt kann [hier](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/config_file_structure) nachgelesen werden, an sich kann jede Einstellung geändert werden. Wie die Information Formatiert sein muss, kann man dann den Vorlagen erkennen.
 
### 3.1 Wichtigste Einstellungen
Es folgen die Wichtigsten Einstellungen die vorzunehmen sind:
 
- "save_path" Pfad zu dem Ordner in dem die savegames gespeichert werden
- "save_game_name" Name des Speicherstandes das ausgelesen werden soll
- "game_path" Pfad zu dem Ordner in dem EU4 installiert ist
- "language" Sprache in der die Daten hochgeladen werden
- "shown_info" Gibt an welche Daten alle in der Tabelle angezeigt werden sollen. Die Reihenfolge in der Liste gibt an, in welcher Reihenfolge Sie in der Tabelle stehen
- "approximation_info" Daten für die ein ungefährer Bereich angegeben werden soll. (müssen auch in "shown_info" vorhanden sein)
- "sheet_Information" zu dem Einstellen der einzelnen Tabellen  
    - der Name der einzelnen Tabellenkonfiguration muss mit einer zuvor erstellten Tabelle übereinstimmen. In diese Tabelle werden die Daten hochgeladen
    - "client_mail" E-Mail des Dienstkontos das in der Tabelle Freigegeben wurde 
    - "percent" gibt an wie groß der ungefähre Bereich für diese Tabelle sein soll
    - "continents" Alle Länder mit der Hauptstadt in diesem Kontinent werden in dieser Tabelle angezeigt
 
 
### 3.2 Andere Einstellungen
- "dev_multi" zusätzliche Konfiguration für die "shown_info" "subject_dev". Ist ein Multiplikator mit dem die Entwicklung der Untertanen multipliziert wird
- "tributary_include" zusätzliche Konfiguration für die "shown_info" "subject_dev". Gibt an ob auch Tributstaaten mit einbezogen werden sollen
 
- "is_trade_sheet" gibt an ob noch ein zusätzliches Tabellenblatt mit Handelsinformationen hinzugefügt werden soll (Prozentualer Anteil für den Handelsbonus) 
 
- "standard_deviation" gibt an wie stark die änderungen der ungefähren Werte von Update zu Update sind. Auch wenn die Daten exakt gleich bleiben, beeinflusst dieser Wert, ob und wie stark sich der ungefähre Bereich ändern kann. Bei 0 ändert sich nichts und je größer dieser ist, desto stärker ist der Effekt (10 empfohlen). Weitere Informationen findest du [hier](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/Approximation.md).
 
 
 
## 4. Start
Um das Programm zu verwenden muss die Datei "app.exe" ausgeführt werden. Im Anschluss öffnet sich ein Fenster mit einem Dialog,
Wähle die gewünschte Konfiguration, mit seiner Nummer, aus und drücke Enter.
 
Es wird Initial zum Start des Programms die Tabelle aktualisiert, sowie wenn sich das ausgewählte savegame geändert (gespeichert) wurde.
 
Hiernach läuft alle automatisch ab. In dem Fenster wird im Anschluss Informationen zu dem Verlauf des Programms gezeigt, sowie
gegebenenfalls Fehler.

