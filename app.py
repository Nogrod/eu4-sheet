import itertools
import logging
import time
from concurrent import futures
import sys

from src import helpers
from src.upload import google
from src.data_processing import rawData, sheet, trade
from src.configuration import mapInformation, config, check_user_settings, translation

__version__ = "v2.2.8.1"
__author__ = 'Manuel Luther'
__licence__ = 'MIT License'


def main():
    initialize()
    old_file_time = 0  # Timestamp, when the last time the file was modified
    country_data_list = None  # list with all country data. needed in the next iteration for approximated data
    while True:
        # update data, when the file change

        new_file_time = helpers.get_save_game_file_time()

        if new_file_time == old_file_time:
            # file was not changed
            time.sleep(0.1)
        else:
            logging.info("------- Save game changed, start update -------")
            old_file_time = new_file_time

            start_time = time.time()
            uploaded_successful, country_data_list = update_sheets(country_data_list)
            end_time = time.time()

            if uploaded_successful:
                logging.info(f"successfully FINISHED in {round(end_time - start_time, 3)} seconds")

            logging.info("Wait for next save game change")


def update_sheets(country_data_list=None) -> (bool, dict):
    """
    :param country_data_list:
    :return:
    """
    try:
        # get data from save game
        country_data_list, date = rawData.get_country_data(country_data_list)

        if config.user.is_trade_sheet:
            trade.add_trade_data(country_data_list)

    except Exception as exception:
        logging.error("===========================================")
        logging.exception(str(exception))
        logging.error("Something went wrong. An error has occurred")
        logging.error("Skip this update and wait for next file change")
        logging.error("===========================================")
        return False, None

    # ==========================================================================
    # upload data to online sheet

    with futures.ThreadPoolExecutor() as executor:
        executor.map(google.upload_data,
                     zip(sheet.sheet_list, itertools.repeat(country_data_list), itertools.repeat(date)))

    if config.is_to_many_requests:
        config.is_to_many_requests = False  # reset value
        logging.info("Because too many requests were sent, "
                     "the program will WAIT at least 120 SECONDS until the next update.")
        return False, country_data_list

    if config.error_occurred:
        logging.info("=====================================" * 2)
        logging.info("=====================================" * 2)
        logging.info("An ERROR has occurred. The program can not recover!")
        logging.info("STOP Program.")
        logging.info("=====================================" * 2)
        logging.info("=====================================" * 2)
        sys.exit(-1)
    else:
        return True, country_data_list


def initialize():
    config.set_general_information()  # need to be before user_configuration
    config.initialize_user_configuration()  # need to be at "first", this data is basically needed everywhere
    mapInformation.initialize_map_information()
    check_user_settings.check_user_input()  # need to be after initialize_map_information

    sheet.initialize_sheets()  # need to be after mapInformation and check_user_input
    translation.initialize_translation()
    config.set_trade_good_names()


if __name__ == "__main__":
    main()
