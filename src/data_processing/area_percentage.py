import re

from src.configuration import mapInformation


def add_region_percentage(save_game: list, country_list: dict):
    """
    add to every country data for the area_dev attribute
    :param save_game: save game to extract the data
    :param country_list: list of all country
    :return:
    """
    provinces = get_province_list(save_game)
    current_region_idx = 1

    while current_region_idx < len(provinces):
        line = provinces[current_region_idx]

        province_id = re.match(r'-(\d*)={\n', line).group(1)
        region_name = get_region_from_province(province_id)

        next_province = provinces.index('\t}\n', current_region_idx)
        dev, tag = get_province_information(provinces[current_region_idx:next_province])

        if tag and tag in country_list:
            country_list[tag].region_dev[region_name] += dev

        current_region_idx = next_province + 1


def get_province_list(save_game) -> list:
    """
    return a list where just contains the province information
    :param save_game:
    :return:
    """
    start_idx = save_game.index("provinces={\n")
    end_idx = save_game.index("}\n", start_idx)
    return save_game[start_idx:end_idx]


def get_region_from_province(province_id: str) -> str:
    """
    get the region name from the given province
    :param province_id:
    :return: region name
    """
    for area, province_ids in mapInformation.region_id.items():
        if province_id in province_ids:
            return area


def get_province_information(lines: list) -> (int, str):
    """
    get the development and the owner of the given province
    :param lines: province information to the province
    :return: development, owner (tag)
    """

    patterns = ["\t\tbase_production=", "\t\tbase_tax=", "\t\tbase_manpower="]

    num_matches = 0  # count the number of information where get found
    development = 0  # dev per province
    tag = ""
    for line in lines:
        for pattern in patterns:
            if pattern in line:
                development += float(line.strip().split("=", 1)[1])
                num_matches += 1
                break

        if "\t\towner=" in line:
            tag = line.strip().split("=", 1)[1].replace('"', '')

        if num_matches == 3:
            break

    return development, tag
