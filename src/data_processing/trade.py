import operator


def add_trade_data(country_data_list: dict):
    """
    adds to every country the trade data
    :param country_data_list:
    :return:
    """

    global_trade = calculate_global_trade(country_data_list)

    for key, country in country_data_list.items():
        country.calculate_trade(global_trade)


def calculate_global_trade(country_data_list: dict) -> list:
    """
    calculates the overall global trade
    :param country_data_list:
    :return:
    """
    country_data = iter(country_data_list.values())
    global_trade = [int(val) for val in next(country_data).data["traded"]]
    for country in country_data:
        global_trade = map(operator.add, global_trade, country.data["traded"])
    global_trade = list(global_trade)

    return global_trade
