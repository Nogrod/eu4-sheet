import datetime
import re

from src.configuration import data_names


def legitimacy_crap(relevant_data: dict):
    """
    depends on the government type a nation can either have on of type of all_type (see below)
    in the memory state there is the case that two or more of these types are stored with one value.
    Only one of which can be non-zero. This value is determined and set
    :param relevant_data:
    :return:
    """
    if relevant_data["reforms"][0] == "native_basic_reform":
        # this government type has no legitimacy or something similarly
        return

    legitimacy_com_value = "0"

    # all possible types of legitimacy
    all_types = {"legitimacy", "republican_tradition", "republican_tradition", "horde_unity", "devotion"}
    intersection_types = set(relevant_data.keys()).intersection(all_types)

    for curr_type in intersection_types:
        if float(relevant_data[curr_type]) != 0.0:
            legitimacy_com_value = relevant_data[curr_type]
            break

    if relevant_data["reforms"][0] == 'theocracy_mechanic' and "devotion" not in relevant_data:
        legitimacy_com_value = "100"

    relevant_data["legitimacy_combined"] = legitimacy_com_value


def get_truces(country_data: list, date: str, idx: int, relevant_data: dict) -> int:
    """
    Search through the country data for past wars, looking for wars where still is a truce.
    append relevant_data with current truces, by the key "truces"
    :param country_data:
    :param date:
    :param idx:
    :param relevant_data:
    :return:
    """
    end_index = country_data.index("\t\t}\n", idx)
    idx -= 1

    list_truce = []
    relevant_data["truces"] = list_truce

    while idx + 1 < end_index:
        idx += 1
        line = country_data[idx]

        match = re.search('([ \t]*)([A-Z]{3})(={)', line)

        if not match:
            continue

        tag = match.group(2)
        last_war = ""
        last_ware_sore = 0
        is_truce = False
        # search in this relation after "last_warscore" anc "last_war"
        while line != "\t\t\t}\n":
            idx += 1
            line = country_data[idx]
            if "last_warscore=" in line:
                last_ware_sore = int(line.strip().split("=")[1])
            elif "last_war=" in line:
                last_war = datetime.datetime.strptime(line.strip().split("=")[1], '%Y.%m.%d')
            elif "truce=yes" in line:
                is_truce = True

        # check if there is a last_war with this relation, then calculate truce end, from war
        if last_war and is_truce:
            truce_end = calculate_truces(last_ware_sore, last_war)
            # check, if the truce end is in the future, then add to list
            if date < truce_end:
                list_truce.append([tag, truce_end])

    return end_index


def calculate_truces(last_war_score: int, last_war: datetime) -> str:
    """
    calculates the end date of the end of a ceasefire
    :param last_war_score:
    :param last_war:
    :return:
    """
    truce_years = 5 + 0.1 * last_war_score
    truce_days = truce_years * 365
    truce_end = last_war + datetime.timedelta(days=truce_days + 63)
    return truce_end.strftime('%Y.%m')


def enemy(country: list, idx: int, country_data: dict):
    """
    get all enemy's from the country
    :param country:
    :param idx:
    :param country_data:
    :return:
    """
    line = country[idx]
    enemy_list = []
    while "enemy" in line:
        enemy_list.append(line.split('"')[1].strip())
        idx += 1
        line = country[idx]
    country_data["enemy"] = enemy_list
    return idx


class MultiLineMethods:
    """
    methods where proceed data which extend over several lines and which must be handled in a custom manner.
    """

    # Methods get called over getattr()

    @staticmethod
    def active_idea_groups(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        return MultiLineMethods.Helper.get_information(country, idx, country_data, name_info_block, date)

    @staticmethod
    def technology(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        return MultiLineMethods.Helper.get_information(country, idx, country_data, name_info_block, date)

    @staticmethod
    def rival(country: list, idx: int, country_data: dict, name_info_block: str, date: str):

        tag = country[idx + 1].split('"')[1]

        if name_info_block not in country_data:
            country_data[name_info_block] = []

        country_data[name_info_block].append(tag)
        return idx + 3

    @staticmethod
    def monarch(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        # get character id_char
        id_char = country[idx + 1].split('=')[1].strip()
        MultiLineMethods.Helper.get_character_info(country, date, id_char, country_data, "monarch")

        return idx + 3

    @staticmethod
    def heir(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        # get character id_char
        id_char = country[idx + 1].split('=')[1].strip()

        MultiLineMethods.Helper.get_character_info(country, date, id_char, country_data, "heir")

        return idx + 3

    @staticmethod
    def queen(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        # get character id_char
        id_char = country[idx + 1].split('=')[1].strip()

        MultiLineMethods.Helper.get_character_info(country, date, id_char, country_data, "queen")

        return idx + 3

    @staticmethod
    def reforms(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
        # get government type and reforms
        idx += 1
        line = country[idx]

        country_data[name_info_block] = list()
        while '\t\t\t\t}\n' not in line:
            line = line.strip().replace('"', '')
            country_data[name_info_block].append(line)

            idx += 1
            line = country[idx]

        return idx + 2

    class Helper:
        """
        several methods from the Multiline class uses these functions to proceed data
        """

        @staticmethod
        def get_information(country: list, idx: int, country_data: dict, name_info_block: str, date: str):
            # used by active_idea_groups and technology
            country_data[name_info_block] = []
            idx += 1
            line = country[idx]
            while "\t\t}\n" != line:
                country_data[name_info_block].append(line.strip().split("="))
                idx += 1
                line = country[idx]
            return idx

        @staticmethod
        def get_character_info(country: list, date: str, id_char: str, relevant_data: dict, mode: str):
            """
            add to the country_data dict the relevant information to the character
            :param country: raw data
            :param date: current game date
            :param id_char: id from the character
            :param relevant_data:
            :param mode: it is a monarch or a heir
            :return:
            """
            if not id_char:
                raise ValueError

            # get the index (idx) where the character information begins
            try:
                idx = 1
                while True:
                    # could fail when the country is in a Pu (ValueError)
                    idx = country.index(f"\t\t\t\t\t\tid={id_char}\n", idx)
                    # break up, if character is a heir oder a monarch (48 is the type for monarch or a heir)
                    if country[idx + 1].split("=")[1].strip() == "48":
                        break
                    idx += 1

                relevant_data[mode] = {}
                relevant_data[mode]["id"] = id_char
            except ValueError:
                missing_ruler.add(relevant_data["overlord"].replace('"', ''))
                return

            # collect data from the character
            regent = False
            while country[idx + 1] != "\t\t\t\t}\n":
                idx += 1
                line = country[idx]

                # check if a relevant information is in the line
                if line.startswith(data_names.character_in_line):
                    var, val = line.split("=")

                    if '"' in line:
                        val = line.split('"')[1]

                    # set relevant information
                    relevant_data[mode][var.strip()] = val.strip()
                    continue

                # check if a relevant information is in the line
                if line == data_names.character_multi_line:
                    name = line.split("=")[0].strip()
                    relevant_data[mode][name] = []

                    while country[idx + 1] != "\t\t\t\t\t}\n":
                        idx += 1
                        line = country[idx]
                        # set relevant information
                        relevant_data[mode][name].append(line.split("=")[0].strip())
                    continue

                # check if the character is a regent
                if "\t\t\t\t\tregent=yes\n" == line:
                    regent = True

            # calculate age
            relevant_data[mode]["age"] = MultiLineMethods.Helper.calculate_age(relevant_data[mode]["birth_date"], date)

            # check if character is a regent
            if regent:
                relevant_data[mode]["regent"] = True

        @staticmethod
        def calculate_age(birth: str, date: str) -> int:
            """
            calculates the age from an character
            :param birth:
            :param date:
            :return:
            """
            date = date.split(".")[0]
            birth = birth.split(".")[0]
            return int(date) - int(birth) - 1


missing_ruler = set()  # A subject of this nation miss ruler information
