import itertools
import logging
import sys
from concurrent import futures

from src.configuration import mapInformation, config
from src.upload.google import clear_sheet

sheet_list = []


class Sheet:

    def __init__(self, sheet_name: str, client_mail: str, percent: str, continents: list, super_regions: list, regions: list):
        self.cred = config.file_name_credentials[client_mail]
        self.percent = int(percent)
        self.sheet_name = sheet_name
        self.client_mail = client_mail
        self.row_count = None

        # collect all continent provincial ids for this table
        continent_provinces = [mapInformation.continent_id[continent_name] for continent_name in continents]

        # collect all region provincial ids for this table
        region_provinces = [mapInformation.region_id[region_name] for region_name in regions]

        # collect all super-region provincial ids for this table
        super_region_provinces = [mapInformation.super_region_id[s_r_name] for s_r_name in super_regions]

        # collect all provincial ids for this table
        self.provinces = {province for province in itertools.chain(*continent_provinces, *region_provinces, *super_region_provinces)}


def initialize_sheets():
    """
    create sheets after the custom definition in the config file
    save sheets in the "sheet_list" global variable
    :return:
    """
    # for sheet_name, sheet_information in config.user.sheet_Information.items():
    #    sheet = Sheet(sheet_name=sheet_name, **sheet_information)
    #    sheet_list.append(sheet)
    #    clear_sheet(sheet)

    with futures.ThreadPoolExecutor() as executor:
        executor.map(init_sheet, config.user.sheet_Information.items())

    if config.error_occurred:
        logging.info("=====================================" * 2)
        logging.info("An ERROR has occurred. The program can not recover!")
        logging.info("STOP Program.")
        logging.info("With the initialization of your settings. Look further up in the logs for an error.")
        logging.info("=====================================" * 2)
        sys.exit(-1)


def init_sheet(sheet_information):
    sheet_name, sheet_data = sheet_information
    sheet = Sheet(sheet_name=sheet_name, **sheet_data)
    sheet_list.append(sheet)
    clear_sheet(sheet)


if __name__ == "__main__":
    initialize_sheets()
