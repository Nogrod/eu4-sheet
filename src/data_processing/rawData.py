import collections
import io
import os

from src.configuration import data_names, config, translation
from src.data_processing.Country import Country
from src.helpers import convert_date_format
from src.data_processing.area_percentage import add_region_percentage
from src.data_processing.parser_function import legitimacy_crap, get_truces, enemy, MultiLineMethods, missing_ruler


def get_country_data(country_list: collections.defaultdict) -> (dict, str):
    """
    return a list with country objects and the current in game date
    :param country_list:
    :return:
    """
    if country_list is None:
        country_list = collections.defaultdict(Country)  # list with country data

    # get save game
    save_game = read_save_game()

    date = convert_date_format(save_game[1])

    # start and begin index from all countries Information
    start_index = save_game.index("countries={\n")
    end_index = save_game.index("active_advisors={\n") - 1

    country_start_index = start_index + 1

    while country_start_index < end_index:
        # set new index where the current country end
        country_end_index = save_game.index("\t}\n", country_start_index) + 1

        # get the tag from the current country
        tag = save_game[country_start_index].split("=")[0].strip()

        # check if country exist. yes -> append to list
        continent_index = save_game.index("\t\tcontinent={\n", country_start_index) + 1

        if not check_country_exist(save_game[continent_index]):
            # country do not exist anymore
            # check if the country is in the list: yes -> remove it
            if tag in country_list:
                del country_list[tag]

            # set new index, where the next country start
            country_start_index = country_end_index
            continue

        # get the relevant data from the country
        needed_data = filter_country_data(save_game[country_start_index:country_end_index], date, tag)

        country_list[tag].update_data(needed_data, tag)

        # set new index, where the next country start
        country_start_index = country_end_index

    # add additional data ========================================================
    if "per_region_dev" in config.user.shown_info:
        add_region_percentage(save_game, country_list)
        for tag, country in country_list.items():
            country.add_region_percentage()

    if "subject_dev" in config.user.shown_info:
        for tag, country in country_list.items():
            country.add_subject_dev(country_list)

    add_missing_ruler(country_list)

    return country_list, date


def add_missing_ruler(country_list):
    """
    sometimes if a country is in a pu the monarch information missing
    here the information will be added
    :param country_list:
    :return:
    """
    for country_tag in missing_ruler:
        country = country_list[country_tag]
        if "subjects" not in country.data:
            continue

        for subject in country.data["subjects"]:
            subject = country_list[subject]
            if 'monarch' not in subject.data and 'monarch' in country.data:
                subject.data['monarch'] = country.data["monarch"]
                subject.data['monarch_traits'] = country.data["monarch_traits"]
            if 'heir' not in subject.data and 'heir' in country.data:
                if subject.data["monarch"]["id"] == country.data["monarch"]["id"]:  # check if it is a Pu
                    subject.data['heir'] = country.data["heir"]
                    subject.data['heir_traits'] = country.data["heir_traits"]


def read_save_game():
    """
    read in the save game
    :return:
    """
    # path to save game
    path = os.path.join(config.user.save_game_path)
    try:
        with io.open(path, "r", encoding='utf8') as file:
            save_game = file.readlines()
    except UnicodeDecodeError:
        with open(path, "r") as file:
            save_game = file.readlines()
    return save_game


def check_country_exist(line: str) -> bool:
    """
    Check if a Country is on a continent
    :param line: get a string like this: "0 0 1 0"
    :return:
    """
    # if there are one "1" the country exist
    if int(line.strip().replace(" ", "")) == 0:
        return False
    return True


def filter_country_data(country: list, date: str, tag: str) -> dict:
    """
    get just the needed / relevant data from all country data
    :param tag: tag from country
    :param country:
    :param date: current in game date
    :return: relevant data
    """

    relevant_data = {"regiment": 0, "ship": 0}

    # get relevant country information
    idx = 0
    while idx + 1 < len(country):
        idx += 1
        line = country[idx]

        if line.startswith(data_names.in_line):
            var, val = line.split("=")
            relevant_data[var.strip()] = val.strip()
            continue

        # true: in the next line are relevant data
        if line in data_names.next_line:
            relevant_data[line.split("=")[0].strip()] = country[idx + 1].strip().split(" ")
            idx += 2
            continue

        # true: in te next lines are relevant data
        if line in data_names.multi_line:
            name_data = line.strip().split("=")[0]
            idx = getattr(MultiLineMethods, name_data)(country, idx, relevant_data, name_data, date)
            continue

        # true: a counter need to go up
        if line in data_names.counter:
            relevant_data[line.split("=")[0].strip()] += 1
            continue

        # true: begin of the active relations block
        if line == data_names.active_relations:
            idx = get_truces(country, date, idx, relevant_data)
            continue

        # true: name from a country
        if data_names.dynamic_name_nation in line[:len(data_names.dynamic_name_nation)]:
            translation.add_tag_to_translation(tag, line.split('"')[1].strip())

        if data_names.enemy in line:
            idx = enemy(country, idx, relevant_data)

    # true: the country is a human player
    if country[1] == data_names.human_player:
        # add a prefix to the country name
        config.player_tag_list.add(tag)

    # delete national idea group
    del relevant_data['active_idea_groups'][0]

    if 'overlord' in relevant_data:
        relevant_data['overlord'] = relevant_data['overlord'].replace('"', '')

    # add the nested to the first layer
    for name, new_data in data_names.PureNames.special_data.items():
        if name in relevant_data:
            relevant_data[new_data] = relevant_data[name]

    if 'corruption' not in relevant_data:
        relevant_data['corruption'] = "0.0"

    legitimacy_crap(relevant_data)

    # convert trade data from string to float, and remove irrelevant data. (first and the last are no trade goods)
    relevant_data["traded"] = [float(num) for num in relevant_data["traded"][1:-1]]
    return relevant_data


