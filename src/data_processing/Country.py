import math
import random

from src import helpers
from src.configuration import data_names, config, translation
from collections import defaultdict


class Country:

    def __init__(self):
        self.data = defaultdict(_Value)
        self.trade = []
        self.region_dev = defaultdict(float)  # development per region

    def update_data(self, data: dict, tag: str):
        """
        update all data for this country
        :param data:
        :param tag:
        :return:
        """
        self.data["tag"] = tag
        self.region_dev = defaultdict(float)

        for name, val in data.items():

            if name in config.user.approximation_info:
                self.data[name].update(val)
                continue

            self.data[name] = val

    def get_data(self, name: str, percent=None) -> str:
        """
        returns the requested data formatted as string
        :param name: requested data
        :param percent: for approximated data
        :return:
        """
        if name not in self.data:
            return ""

        if name in config.user.approximation_info:
            return str(self.data[name].get_data(percent))

        return _DataToString.convert(name, self.data[name])

    def add_region_percentage(self):
        """
        add to self.data the percentage of the development per region
        :return:
        """
        if "per_region_dev" in self.data:
            del self.data["per_region_dev"]

        for region, dev in self.region_dev.items():
            region = region + "_region"
            region_percentage = round(dev / float(self.data["raw_development"]), 2)
            if "per_region_dev" not in self.data:
                self.data["per_region_dev"] = [(region, region_percentage)]
            else:
                self.data["per_region_dev"].append((region, region_percentage))

    def add_subject_dev(self, country_list: dict):
        """
        calculate the subject dev (just possible after all country objects where created)
        :param country_list:
        :return:
        """
        if "subjects" not in self.data:
            return

        # sum up the development ob the subjects
        subject_dev = 0
        for country_tag in self.data["subjects"]:
            if country_tag not in country_list:
                continue

            country = country_list[country_tag]  # get the country object
            if 'tribute_type' in country.data and not config.user.tributary_include:
                continue  # skip if tributary state and tributary states should not count to the subject dev

            subject_dev += float(country.data['raw_development'])

        self.data["subject_dev"] = config.user.dev_multi * subject_dev

    def calculate_trade(self, global_trade_list):
        """
        calculates the "goods percentage" for each country
        :param global_trade_list:
        :return:
        """
        trade = []
        for trade_value, global_trade_value in zip(self.data["traded"], global_trade_list):
            if not global_trade_value:  # prevent division through 0
                trade.append(0)
                continue

            trade.append(trade_value / global_trade_value)

        self.trade = trade

    def get_capital(self):
        """
        return the capital province id
        :return:
        """
        return self.data["capital"]


class _Value:
    """
    Represents data that get approximated
    """
    data: float
    approximated_data_cache = dict()

    def __init__(self):
        # random number between -1 and 1
        self.shift_value = random.random() * random.choice([-1, 1])

    def update(self, data):
        """
        calculate the new shift value and reset cache for new data
        :param data:
        :return:
        """
        self.data = float(data)
        self.update_shift_value()
        self.approximated_data_cache = {}

    def get_data(self, percent: float) -> (float, float):
        """
        Calculate the approximated data
        :param percent:
        :return:
        """

        # return data if it was already calculated
        if percent in self.approximated_data_cache:
            return self.approximated_data_cache[percent]

        if not percent:
            return self.data

        add_value = self.data * percent / 100

        # calculate approximated data
        under_value = round(self.data - add_value + (add_value * self.shift_value))
        upper_value = math.ceil(self.data + add_value + (add_value * self.shift_value))

        self.approximated_data_cache[percent] = (under_value, upper_value)

        return under_value, upper_value

    def update_shift_value(self):
        """
        calculate a new shift value
        :return:
        """

        offset = random.gauss(0, config.user.standard_deviation / 100)
        if abs(offset + self.shift_value) > 1:
            return

        self.shift_value += offset

        # if shift value over 1, calculate a new shift value
        if abs(self.shift_value) > 1:
            sign = (1, -1)[self.shift_value < 0]
            self.shift_value += sign * (1 - abs(self.shift_value)) * 3


class _DataToString:

    @staticmethod
    def convert(name, data):
        """
        get data and return a str
        :param name: name from the data
        :param data: data it self
        :return: a string or int
        """

        number = helpers.convert_to_number(data)
        if number is not False:
            return int(number)

        if isinstance(data, str):
            if name == "tag" and data in config.player_tag_list:
                player_prefix = " P: "
            else:
                player_prefix = ""
            return f"{player_prefix}{translation.get_translation(data)}"

        if name in data_names.PureNames.next_line:
            return ", ".join([str(translation.get_translation(value)) for value in data])

        return getattr(_DataToString, name)(data)

    @staticmethod
    def monarch(data: dict) -> str:
        if 'regent' in data:
            regent_prefix = "R: "
        else:
            regent_prefix = ""
        return f"{regent_prefix}{_DataToString.char_general(data)}"

    @staticmethod
    def queen(data: dict) -> str:
        return _DataToString.char_general(data)

    @staticmethod
    def queen_traits(data: dict) -> str:
        return _DataToString.char_traits(data)

    @staticmethod
    def monarch_traits(data: dict) -> str:
        return _DataToString.char_traits(data)

    @staticmethod
    def heir(data: dict) -> str:
        return _DataToString.char_general(data)

    @staticmethod
    def heir_traits(data: dict) -> str:
        return _DataToString.char_traits(data)

    @staticmethod
    def technology(data: list) -> str:
        """
        return to the technology data a string
        :param data:
        :return:
        """
        return _DataToString._nested_list(data)

    @staticmethod
    def truces(data: list) -> str:
        """
        return to the truces data a string
        :param data:
        :return:
        """
        return _DataToString._nested_list(data)

    @staticmethod
    def active_idea_groups(data: list) -> str:
        """
        return to the idea groups data a string
        :param data:
        :return:
        """
        return _DataToString._nested_list(data)

    @staticmethod
    def _nested_list(data: list) -> str:
        """
        get a list in the form: [ [name_to_translate, value], ... ] and make it to one string
        :param data:
        :return:
        """
        translations = [f"{translation.get_translation(inner[0])}: {inner[1]}" for inner in data]
        return ", ".join(translations)

    @staticmethod
    def char_general(value: dict) -> str:
        if 'dynasty' in value:
            dynasty = f", {value['dynasty']}"
        else:
            dynasty = ""

        return f"{value['name']}{dynasty}: {value['age']}"

    @staticmethod
    def char_traits(value: dict) -> str:
        stats = " ".join([value["ADM"], value["DIP"], value["MIL"]])

        if "personalities" in value:
            personalities = '; '.join(translation.get_translation(val) for val in value['personalities'])
        else:
            personalities = ""

        return f"{stats}: {personalities}"

    @staticmethod
    def reforms(data: dict) -> str:
        government_type = translation.get_translation(data[0])
        government_reform = [translation.get_translation(reform) for reform in data[1:]]
        government_reform = ", ".join(government_reform)
        return f"{government_type}: {government_reform}"

    @staticmethod
    def per_region_dev(data: list) -> str:
        """
        :param data:
        :return:
        """
        return _DataToString._nested_list(data)
