import logging
import os
import sys
import time
from functools import wraps

from src.configuration import config


def get_save_game_file_time():
    """
    get the time when the save game get last changed
    the save game is specified in the selected user config
    :return:
    """
    save_game_name_tmp = config.user.save_game_name.replace(".eu4", ".tmp")

    try:
        # when the game get save [filename].eu4 is not in the folder instead [filename].tmp
        while save_game_name_tmp in os.listdir(config.user.save_path):
            time.sleep(0.1)  # wait until the game have saved
        file_time = os.path.getmtime(config.user.save_game_path)  # get new modified time (for save game file)

    except FileNotFoundError:
        # for the case if between the check for the .tmp file and to get the new_file_time the file will be replaced
        while save_game_name_tmp in os.listdir(config.user.save_path):
            time.sleep(0.1)  # wait until the game have saved
        file_time = os.path.getmtime(config.user.save_game_path)  # get new modified time (for save game file)

    return file_time


def convert_date_format(date: str) -> str:
    """
    convert date to the format XXXX.XX.XX
    :param date: xxxx.x.x
    :return: xxxx.0x.0x
    """
    date = date.split("=")[1]
    date = date.strip().split(".")
    year = [date[0]]
    # add zeros to moth and day, if needed
    month_day = ["0" + i for i in date[1:] if len(i) < 2]
    year.extend(month_day)
    # convert list to string
    date = ".".join(str(e) for e in year)
    return date


def convert_to_number(value):
    """
    :param value:
    :return: return a float if possible. else return false
    """

    if isinstance(value, str):
        try:
            return float(value)
        except ValueError:
            return False

    if isinstance(value, int) or isinstance(value, float):
        return value
    return False


# used by file google =========================================================
def try_this(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as exception:
            logging.exception(str(exception))
            config.error_occurred = True
            sys.exit(-1)
        return result

    return wrapper


def handle_api_error(exe: Exception, sheet):
    """
    Print error message, set error flag on True (main Thread will stop) and exit thread
    just use in subprocess
    :param sheet: object from type Sheet
    :param exe:
    :return:
    """
    if isinstance(exe.args[0], dict):
        if exe.args[0].get('code', False) == 429:  # to many requests
            handel_api_error(exe, sheet)
        else:
            logging.exception(str(exe))
            config.error_occurred = True
    else:
        logging.exception(str(exe))
        config.error_occurred = True

    sys.exit(-1)


def handel_api_error(error: Exception, sheet):
    """
    handles the http status code 429 error
    :param error:
    :param sheet: object from type Sheet
    :return:
    """
    logging.info("---------------------------------------------" * 2)
    logging.info(f"{str(error)}\n"
                 f"The user with mail {sheet.client_mail} has sent the request\n"
                 f"Consider to add a additional User, if you do not have already 5\n"
                 f"or distribute the users / mails more evenly over the tables")
    logging.info("---------------------------------------------" * 2)
    config.is_to_many_requests = True


def line_checker(func):
    """
    empty rows, if the table has fewer rows than the previous
    For example, old (sheet) 10 rows new 8 rows => the new table is set to 10 rows with the last 2 empty.
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(*args, **kwargs):

        rows = func(*args, **kwargs)

        sheet = args[0]
        if sheet.row_count is None:
            sheet.row_count = len(rows)

        row_diff = sheet.row_count - len(rows)
        sheet.row_count = len(rows)  # set new row count

        if row_diff > 0:
            one_row = ["" for _ in range(len(config.user.shown_info) + 1)]  # get "empty" list with len of columns
            for _ in range(row_diff):  # clear old rows
                rows.append(one_row)

        return rows

    return wrapper
