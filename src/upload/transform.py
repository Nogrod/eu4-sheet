from functools import wraps

from src.configuration import config, translation
from src.helpers import line_checker


@line_checker
def main_data(sheet, countries) -> list:
    """
    return the country data in a two dimensional list
    :param countries: list of objects from type Country
    :param sheet: Object of type Sheet
    :return: two dimensional list [row, row, row, ...]
    """
    # get rows with data
    rows = []
    for country in countries.values():
        # skip countries where should not in the sheet
        if country.get_capital() not in sheet.provinces:
            continue

        # add row
        row = [country.get_data(info, sheet.percent) for info in config.user.shown_info]
        row.insert(0, country.get_data('tag'))
        rows.append(row)

    rows.sort(key=lambda _row: _row[0])  # sort after the first column (country name)

    # getfirst row
    first_row = [translation.get_translation(title) for title in config.user.shown_info]
    first_row.insert(0, translation.get_translation('tag'))

    rows.insert(0, first_row)
    return rows


def trade_data(countries: dict):
    """
    return the trade data in a two dimensional list
    :param countries:
    :return:
    """
    rows = []
    # iterate over all trade good names, and get for every trade good the countries with the highest trade value
    for idx, name in enumerate(config.name_trade_goods):
        row = [translation.get_translation(name)]
        highest = sorted(countries.items(), key=lambda country: country[1].trade[idx], reverse=True)[
                  :config.number_of_trade_columns]
        row.extend([trade_template(country, idx) for country in highest])
        rows.append(row)
    return rows


def trade_template(country, idx: int) -> str:
    """
    convert data to string
    :param country:
    :param idx:
    :return:
    """
    return f"{translation.get_translation(country[0])}: {round(country[1].trade[idx] * 100, 2)}"
