import sys
from os.path import join

import gspread
import logging
from oauth2client.service_account import ServiceAccountCredentials

from src.configuration import config

from src.helpers import handle_api_error, try_this
from src.upload.transform import main_data, trade_data


@try_this
def upload_data(kwargs):
    sheet, country_data, date = kwargs
    spreadsheet = get_spread_sheet(sheet)

    logging.info(f"Upload data for sheet: {sheet.sheet_name}")

    rows = main_data(sheet, country_data)

    update_worksheet(spreadsheet.sheet1, date, rows, len(config.user.shown_info), sheet)

    if config.user.is_trade_sheet:

        trade_rows = trade_data(country_data)

        try:
            # get trade work sheet
            trade_worksheet = spreadsheet.worksheet("Trade")
        except gspread.exceptions.WorksheetNotFound:
            # if no trade worksheet exist, create one
            trade_worksheet = spreadsheet.add_worksheet("Trade", len(rows) + 1, len(rows[0]) + 1)

        # update trade data
        try:
            update_worksheet(trade_worksheet, date, trade_rows, config.number_of_trade_columns, sheet)
        except Exception as exp:
            logging.info(str(exp))

    logging.info(f"Upload finished for sheet {sheet.sheet_name}")


def get_spread_sheet(sheet) -> gspread.Spreadsheet:
    """
    get the spreadsheet
    :param sheet: object from type Sheet
    :return:
    """
    # get connection
    scope = ['https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(join("credentials", sheet.cred), scope)
    client = gspread.authorize(credentials)

    try:
        # download actual spread sheet
        spreadsheet = client.open(sheet.sheet_name)
    except gspread.exceptions.SpreadsheetNotFound:
        handle_api_error(ValueError(f"user '{sheet.sheet_name}' have no acces to sheet '{sheet.sheet_name}'.\n"
                                    f"or the sheet name '{sheet.sheet_name}' is wrong"), sheet)
    except gspread.exceptions.APIError as e:
        handle_api_error(e, sheet)

    return spreadsheet


def update_worksheet(worksheet, date: str, rows: list, column_length: int, sheet):
    """
    upload data to the given worksheet
    :param sheet: just for error handling (object from type Sheet)
    :param column_length:
    :param worksheet: worksheet where get updated (gspread class
    :param date: in game date
    :param rows: data to update. 2-D List. in the inner lists, are the columns from the sheet
    :return:
    """
    column_length += 1  # for additional country column
    # get length and width, from the old spreadsheet

    cell_list = []
    for row in range(0, len(rows)):
        for column in range(0, column_length):
            value = rows[row][column]
            cell_list.append(gspread.Cell(row=row + 1, col=column + 1, value=value))

    # set cell with the in game date
    cell_list.append(gspread.Cell(1, column_length + 2, date))

    # upload data
    try:
        worksheet.update_cells(cell_list, value_input_option='USER_ENTERED')
    except gspread.exceptions.APIError as e:
        handle_api_error(e, sheet)


def clear_sheet(sheet):
    """
    remove all data from the sheet and the trade sheet
    :param sheet: object from type Sheet
    :return:
    """
    try:
        spreadsheet = get_spread_sheet(sheet)
    except Exception as exception:
        if exception.args[0] == 'invalid_grant: Invalid JWT Signature.':
            logging.info(f"invalid credentials. Check the credentials file from the use with the mail {sheet.client_mail}")
            logging.info(f"Consider to create a new credentials file.")
        config.error_occurred = True
        sys.exit(-1)
    spreadsheet.sheet1.clear()

    # clear trade worksheet if present
    for worksheet in spreadsheet.worksheets():
        if worksheet.title == "Trade":
            worksheet.clear()
