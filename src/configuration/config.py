import json
import logging
import os
import sys

from src.configuration.config_class import Config, SupportedConfig

# global Variables / Settings where get used everywhere ========================
user = Config()  # will be overwritten later
supported_config = SupportedConfig()
number_of_trade_columns = 3
file_name_credentials = {}
player_tag_list = set()  # list with tags from all nation where are played by humans
name_trade_goods = []
error_occurred = False  # flag if a error occurs during the upload (multiprocessing)
is_to_many_requests = False

logging.basicConfig(format="%(asctime)s: %(message)s", level=logging.INFO, datefmt="%H:%M:%S")


def set_user_config():
    """
    get all user configurations (from config.yml) and save them in the global variable user
    :return:
    """

    config_path = os.path.join("configurations", get_file_from_user())
    print(f"{config_path} is selected")

    global user
    user = Config(config_path)


def get_file_from_user() -> str:
    """
    the user can select a config file
    :return: file name from the config file
    """
    config_files = os.listdir("configurations")

    config_files = [file for file in config_files if file.endswith(".yml")]

    for idx, file in enumerate(config_files):
        print(f"{idx + 1}) {file}")

    while True:
        print("Choose one configuration file. Type the number from the file and press enter to continue:")
        file_number = input()
        try:
            file_number = int(file_number)
        except ValueError:
            print("Pls type in a valid number")
            continue
        if 0 < file_number <= len(config_files):
            return config_files[file_number - 1]

        print("Pls type in a valid number")


def set_trade_good_names():
    """
    extends the global variable name_trade_goods with all available trade good names in eu4
    :return:
    """
    with open(os.path.join(user.game_path, "common", "tradegoods", "00_tradegoods.txt"), "r") as file:
        file = file.readlines()

    trade_good_list = []

    for line in file:
        if len(line) < 3:
            continue

        if line[0] == "#" or line[0] == "\t":
            continue

        if "}" in line:
            continue

        trade_good_list.append(line.split("=")[0].strip())

    # [:-1] because, the first and the last, are no trade goods
    name_trade_goods.extend(trade_good_list[:-1])


def get_cred():
    """
    set data in user_credentials
    :return: key e_mail value file name from the needed credentials
    """

    # get credentials file
    cred_folder = "credentials"

    cred_folder = os.path.join("", cred_folder)

    files = os.listdir(cred_folder)
    credentials_files = [i for i in files if ".json" in i]

    # get file name to user
    for name in credentials_files:
        with open(f'{os.path.join(cred_folder, name)}') as json_data:
            file = json.load(json_data)

        if file["client_email"] in file_name_credentials:
            logging.info(f"There are two credential files with the same email. Remove one of these files")
            logging.info(f"email: {file['client_email']}")
            logging.info("=====================================" * 2)
            logging.info("An ERROR has occurred. The program can not recover!")
            logging.info("STOP Program.")
            logging.info("=====================================" * 2)
            sys.exit(-1)

        file_name_credentials[file["client_email"]] = name


def get_game_map_path() -> str:
    """
    :return: absolute path to the map folder in the EU4 files
    """
    if not user:
        raise Exception("User Data has to be set before, this function get called")
    return os.path.join(user.game_path, "map")


def set_general_information():
    global supported_config
    supported_config = SupportedConfig("base_config_data.yml", )


def initialize_user_configuration():
    set_user_config()
    get_cred()


if __name__ == "__main__":
    set_general_information()
    initialize_user_configuration()
