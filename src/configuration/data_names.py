# list of names to extract the data from the save games

# in the same line where the  string stand is the information
in_line = ("\t\tmax_manpower=", "\t\traw_development=", "\t\testimated_monthly_income=", "\t\treligion=",
           "\t\toverlord=", "\t\tmanpower=", "\t\tsailors=", "\t\tmax_sailors=", "\t\tcapital=",
           "\t\ttribute_type=", "\t\tinnovativeness=", "\t\tstability=", "\t\tprestige=", "\t\tcorruption=",
           "\t\tlegitimacy=", "\t\trepublican_tradition=", "\t\trepublican_tradition=", "\t\thorde_unity=",
           "\t\tdevotion=", "\t\tabsolutism=")

# a block of information begins
multi_line = ("\t\tactive_idea_groups={\n", "\t\trival={\n", "\t\ttechnology={\n", "\t\tmonarch={\n", "\t\tqueen={\n",
              "\t\their={\n", "\t\t\t\treforms={\n")

# relevant data all in the next line
next_line = ("\t\tsubjects={\n", "\t\tallies={\n", "\t\ttrade_embargoes={\n", "\t\ttraded={\n")

enemy = "\t\tenemy="

# a counter need to go up
counter = ("\t\t\tship={\n", "\t\t\tregiment={\n")

# for names from created nations
dynamic_name_nation = "\t\tname="

# human player
human_player = "\t\thuman=yes\n"

# for truces
active_relations = "\t\tactive_relations={\n"

# for character info
character_in_line = ("\t\t\t\t\tDIP=", "\t\t\t\t\tADM=", "\t\t\t\t\tMIL=", "\t\t\t\t\tdynasty=", "\t\t\t\t\tname=",
                     "\t\t\t\t\tbirth_date=", "\t\t\t\t\tdynasty=")

# special case from multi_line
character_multi_line = "\t\t\t\t\tpersonalities={\n"


class PureNames:
    # Names from above, but without "=" and "\n" and ...

    in_line = [name.strip().split("=")[0] for name in in_line]
    multi_line = [name.strip().split("=")[0] for name in multi_line]
    next_line = [name.strip().split("=")[0] for name in next_line]
    next_line.append("rival")
    next_line.append("enemy")
    counter = [name.strip().split("=")[0] for name in counter]

    # data created from existing data
    special_data = {"monarch": "monarch_traits",
                    "heir": "heir_traits",
                    "queen": "queen_traits"}
