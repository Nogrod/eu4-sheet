import os
import re
import sys

from src.configuration import mapInformation, config


def check_user_input():
    """
    check if the configuration from the user in the config text file is correct
    raise Error if not
    :return:
    """
    is_config_correct = True
    # check if the user have typed in valid names for shown_info
    for data_name in config.user.shown_info:
        if data_name not in config.supported_config.shown_info:
            print(f'shown_info: "{data_name}" is not a valid shown_info. look in the file base_config_data.yml, for a correct option')
            is_config_correct = False

    # check if the user have typed in valid names for approximation_info
    for data_name in config.user.approximation_info:
        if data_name not in config.supported_config.approximation_info:
            print(f'approximation_info: "{data_name}" is not a valid approximation_info. look in the file base_config_data.yml, for a correct option')
            is_config_correct = False

    # check if the user have typed in a valid languages
    if config.user.language not in config.supported_config.languages:
        print(f'language: "{config.user.language}" is a invalid language. Choose one of the following: '
              f'{config.supported_config.languages}')
        is_config_correct = False

    # check if the paths exist =========================================================================================
    if not os.path.isdir(config.get_game_map_path()):
        print(f'game_path: Invalid Path "{config.get_game_map_path()}"')
        is_config_correct = False

    if not os.path.isfile(config.user.save_game_path):
        print(f'save_path: Invalid Path "{config.user.save_game_path}"')
        is_config_correct = False

    # check if the values have a correct type ==========================================================================

    if not (isinstance(config.user.dev_multi, float) or isinstance(config.user.dev_multi, int)):
        print(f'dev_multi: {config.user.dev_multi} is not a valid number')
        is_config_correct = False

    if not (isinstance(config.user.standard_deviation, float) or isinstance(config.user.standard_deviation, int)):
        print(f'standard_deviation: {config.user.standard_deviation} is not a Valid number')
        is_config_correct = False

    if not isinstance(config.user.tributary_include, bool):
        print(f'tributary_include: {config.user.tributary_include} is not "True" or "False"')
        is_config_correct = False

    if not isinstance(config.user.is_trade_sheet, bool):
        print(f'is_trade_sheet: {config.user.is_trade_sheet} is not "True" or "False"')
        is_config_correct = False

    # check if sheet_Information is correct ============================================================================
    for sheet_name, sheet in config.user.sheet_Information.items():
        # General Email Regex (RFC 5322 Official Standard)
        if not re.match(
                r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", sheet["client_mail"]):
            print(f'{sheet_name}: client_mail: "{sheet["client_mail"]}" is not a valid mail')
            is_config_correct = False

        if sheet["client_mail"] not in config.file_name_credentials:
            print(f'{sheet_name}: no credentials are stored for the e-mail {sheet["client_mail"]}')
            is_config_correct = False

        # check if the user have typed in valid continents
        for continent_name in sheet["continents"]:
            if continent_name not in mapInformation.continent_id.keys():
                print(f'{sheet_name}: continents: "{continent_name}" is not a valid continent. look in the file base_config_data.yml, for a correct option')
                is_config_correct = False

        # check if the user have typed in valid regions
        for region_name in sheet["regions"]:
            if region_name not in mapInformation.region_id.keys():
                print(f'{sheet_name}: regions: "{region_name}" is not a valid continent. look in the file base_config_data.yml, for a correct option')
                is_config_correct = False

        for super_region_name in sheet["super_regions"]:
            if super_region_name not in mapInformation.super_region_id.keys():
                print(f'{sheet_name}: super_regions: "{super_region_name}" is not a valid super_region. look in the file base_config_data.yml, for a correct option')
                is_config_correct = False

        # check type
        if not (isinstance(sheet["percent"], float) or isinstance(sheet["percent"], int)):
            print(f'{sheet_name}: percent: "{sheet["percent"]}" is not a valid number')
            is_config_correct = False

    if not is_config_correct:
        print("==========================================================")
        print("Invalid config (see above). EXIT program!")
        sys.exit(-1)
