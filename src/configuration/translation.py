import codecs
import os

import yaml

from src.configuration import config

translations = dict()


def get_translation(name: str) -> str:
    try:
        return translations[name]
    except KeyError:
        return name


def initialize_translation():
    language = config.user.language
    # get all translation from the game
    load_game_translations(language)

    # get all translation from "customTranslations.yml" and overwrite eventually translations from the game
    load_custom_translation(language)


def load_game_translations(language: str):
    """
    load every translation file from the given language
    :param language: language to translate
    :return:
    """
    loc_path = os.path.join(config.user.game_path, "localisation")
    files = os.listdir(loc_path)

    for file in files:
        if not file.endswith(f"_{language}.yml"):  # skip if it is not a file with the correct language
            continue

        with codecs.open(os.path.join(loc_path, file), "r", "utf-8") as f:
            lines = f.readlines()

        # parse a file
        for line in lines[1:]:
            if line.strip().startswith("#"):
                continue

            if len(line) <= 3:
                continue

            key, value = line.split(":", 1)
            key = key.strip()

            if key[0] == "#":
                continue

            value = value.split('"')[1].strip()
            translations[key] = value


def load_custom_translation(language: str):
    """
    load the custom translation
    :param language:
    :return:
    """
    with codecs.open("customTranslations.yml", "r", "utf-8") as file:
        custom_translation = yaml.safe_load(file)
        for key, c_translations in custom_translation.items():
            if key.startswith(language):
                translations.update(c_translations)


def add_tag_to_translation(tag: str, name: str):
    """
    update the name from a country
    (e.g. for colonial nations where the name is custom)
    :param tag:
    :param name:
    :return:
    """
    translations[tag] = name


if __name__ == "__main__":
    initialize_translation()
