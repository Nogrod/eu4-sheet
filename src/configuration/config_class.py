import os
import sys

import yaml


class Config:
    save_game_path: str
    save_game_name: str
    save_path: str
    game_path: str
    language: str
    shown_info: list
    approximation_info: list
    dev_multi: float
    tributary_include: bool
    is_trade_sheet: bool
    standard_deviation: float
    sheet_Information: dict

    def __init__(self, yaml_file_path=None):
        """
        :param yaml_file_path: path to the Config file to read in the Config
        """
        if yaml_file_path is None:
            return

        yaml_config = Config._red_yaml_file(yaml_file_path)

        self.save_game_path = os.path.join(yaml_config["save_path"], yaml_config["save_game_name"])
        self.save_game_name = yaml_config["save_game_name"]
        self.save_path = yaml_config["save_path"]
        self.game_path = yaml_config["game_path"]
        self.language = yaml_config["language"]
        self.shown_info = yaml_config["shown_info"]
        self.approximation_info = yaml_config["approximation_info"]
        self.dev_multi = yaml_config["dev_multi"]
        self.tributary_include = yaml_config["tributary_include"]
        self.is_trade_sheet = yaml_config["is_trade_sheet"]
        self.standard_deviation = yaml_config["standard_deviation"]
        self.sheet_Information = yaml_config["sheet_Information"]

    @staticmethod
    def _red_yaml_file(path):
        with open(path, "r") as stream:
            try:
                return yaml.safe_load(stream)

            except yaml.YAMLError as exc:
                print("The selected config file is not valid. Fix the file to continue!")
                print(str(exc))
                sys.exit(-1)


class SupportedConfig:
    languages: list
    shown_info: list
    approximation_info: list
    continents: list
    super_regions: list
    regions: list

    def __init__(self, yaml_file_path=None):
        """
        :param yaml_file_path: path to the Config file to read in the Config
        """
        if yaml_file_path is None:
            return

        yaml_config = SupportedConfig._red_yaml_file(yaml_file_path)

        self.languages = yaml_config["supported languages"]
        self.shown_info = yaml_config["supported data"]
        self.approximation_info = yaml_config["supported approximations"]
        self.continents = yaml_config["continents"]
        self.super_regions = yaml_config["super_regions"]
        self.regions = yaml_config["regions"]

    @staticmethod
    def _red_yaml_file(path):
        with open(path, "r") as stream:
            return yaml.safe_load(stream)
