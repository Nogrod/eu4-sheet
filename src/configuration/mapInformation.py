import os
from collections import defaultdict

from src.configuration import config

continent_id = defaultdict(list)
area_id = {}
region_id = {}
super_region_id = defaultdict(list)


def get_cont_id():
    """
    get all province id's to a continent
    set the information to continent_id
    :return:
    """
    path_cont_file = os.path.join(config.get_game_map_path(), "continent.txt")

    with open(path_cont_file, "r") as stream:
        file = stream.readlines()

    current_cont = ""

    for line in file:

        line = line.strip()

        # begin of a new continent
        if line.endswith(" = {"):
            current_cont = line.split(" = {")[0].strip()

            if "island_check_provinces" == current_cont:
                break
            continue

        if not line or line[0] == '#' or line[0] == '}':
            continue

        # get province ids from line, without commands
        province_string = line.split("#")[0].strip()
        continent_id[current_cont].extend(province_string.split(" "))


def get_super_region_id():
    """
    get all province id's to all super-regions
    set the information to super_region_id
    :return:
    """
    path_area = os.path.join(config.get_game_map_path(), "superregion.txt")

    with open(path_area, "r") as stream:
        super_region_file = stream.readlines()

    current_super_region = ""

    for line in super_region_file:

        line = line.split("#")[0]
        line = line.strip()

        # begin of a new super region
        if line.endswith(" = {"):
            current_super_region = line.split(" = {")[0].strip()
            continue

        if not line or line[0] == '#' or line[0] == '}' or "restrict_charter" in line:
            continue

        # get province ids from line, without commands
        region = line.split("#")[0].strip().replace("_region", "")
        super_region_id[current_super_region].extend(region_id[region])


def get_region_id():
    """
    get all province id's to all regions
    set the information to region_id
    :return:
    """
    path_area = os.path.join(config.get_game_map_path(), "region.txt")

    with open(path_area, "r") as stream:
        region_file = stream.readlines()

    region_name = ""

    idx = region_file.index("random_new_world_region = {\n")

    while idx + 1 < len(region_file):
        idx += 1
        line = region_file[idx].strip()

        # lines with no relevant information
        if len(line) <= 1 or line[0] == '#' or line == "areas = {":
            continue

        # lines with no relevant information
        if line == "monsoon = {":
            idx = region_file.index("}\n", idx)
            continue

        # begin of a new region
        if " = " in line and " {" in line:
            region_name = line.split("=")[0].strip().replace("_region", "")
            region_id[region_name] = []
            continue

        region_id[region_name].extend(area_id[line])


def get_area_id():
    """
    get all province id's to all areas
    set the information to area_id
    :return:
    """
    # add areas to an continent list, where not on the continent
    path_area = os.path.join(config.get_game_map_path(), "area.txt")

    with open(path_area, "r") as stream:
        area_file_full = stream.readlines()

    deprecated_index = area_file_full.index("#Deprecated:\n")
    area_file = area_file_full[:deprecated_index]

    idx = 0
    while idx + 1 < len(area_file):
        idx += 1
        line = area_file[idx].strip()

        # lines with no relevant information
        if len(line) <= 1 or line[0] == '#' or line[0] == "}":
            continue

        # get area name
        area_name = line.split(" ", 1)[0].strip()
        area_id[area_name] = []

        # jump one line further
        if "\tcolor = {" in area_file[idx + 1]:
            idx += 1

        # get province ids from area
        while "}" not in area_file[idx+1]:
            idx += 1
            line = area_file[idx]
            area_id[area_name].extend(line.strip().split(" "))

    for line in area_file_full[deprecated_index:]:
        if "=" in line:
            area_id[line.split("=")[0].strip()] = []


def initialize_map_information():
    get_cont_id()
    get_area_id()
    get_region_id()
    get_super_region_id()
