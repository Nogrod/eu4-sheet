The program offers external statistics for the game Europa Universalis 4, in the form of Google spreadsheets, which can be adjusted as desired and are automatically updated. So you can also show data only approximately, so that only a range will be given where the true value is (e.g. 89-105 with a true value of 100). 
To do this, the program reads a specified savegame (savegame) from EU4 and then uploads the data to at least one table.
[Here](https://docs.google.com/spreadsheets/d/1SHBolh6vFlKhixYmWnT-Q5Y3uO8xQPbIJpNCMECiohM/edit?usp=sharing) and [here](https://docs.google.com/spreadsheets/d/1V3zpvg_MFfk13p8OONNUZvBsvOFjRCuRsHo8f_bH0GE/edit?usp=sharing) you can see two example Sheets.

[German Doku](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/LIESMICH.md)

Information on how the approximation of data works can be found [here](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/Approximation.md)
 
The following explains how to install, configure and use the program:

## 0. download and installation
- You can download it under the following link "Source Code (zip)
    - https://gitlab.com/Nogrod/eu4-sheet/-/releases
- After downloading, the program can be unpacked in any folder using [7-zip](https://www.7-zip.org/) or [winrar](https://www.winrar.de/). (installation completed)
 
## 1. credentials
 
In order for the program to be able to modify the Google spreadsheets that belong to you, the following steps must first be completed.
(a Google account is required)
 
### 1.1 A new project must be created
- For this you need a "project" (at https://console.developers.google.com), for this you can either (if available) use an existing project or create a new one. Under the page you have to create a new project. 
- To create a project go to https://console.developers.google.com and click on "**Select project**" -> "**NEW PROJECT**".
- Give your project a name and create the project. The location is irrelevant. 
 

### 1.2 Add the "Google Drive API" and "Google Sheets API" library to your project
- In order for the program to process the tables, the two libraries mentioned above must be added to the project.
- Make sure that the previously created project is selected. In the top left corner of the screen next to "**GoogleAPIs**" the currently selected project is displayed.
- To add a library you have to search for it first. You can either search via "**Library**" or via the search bar at the top. 
- Then select the library and press "**Activate**". Search and then activate the second library. 
- Now press "**GoogleAPIs**" (top left) and go to the Dashboard, there must be two entries at the bottom of a table named "Google Drive API" and "Google Sheets API".If one or both are missing, you need to activate them as described above.
 

 
### 1.3 Add new credentials
- Now we have to add credentials to allow the program to access the tables later.
- Go to "**Logon data**" => "**Add logon data**" => "**Selection help**". 
 

- Then use the following configuration:
    - Use as API: "Google sheet api
    - Use as platform: "Web server (e.g. Node.js, Tomcat)"
    - Select under "Which data is accessed? ": "Application data" from
    - Select "Do you want to use this API with App Engine or Compute Engine? "No, I do not use it" from
    - Press "What credentials do I need" 

- then we have to configure our service account:
    - Set a name for the service account and assign it the "Editor" role in the "Project
    - Use "JSON" as key type
    - Click on "**Next**" and save the file. Move this file to the folder "credentials" in the program
- (NOTE: the downloaded file must be treated as a password. Keep it secret and DO NOT forward or copy it. The program will only use this file to access the tables specified)
 

Hints:
- the e-mail of this service account is needed at another place. You can find the e-mail either in the downloaded file or on the website https://console.developers.google.com under login details)
 
- The tool uses a service from Google, which is FREE of charge. Google provides a quota per second for this, if the tool exceeds this quota, an error message is displayed and the program crashes. There are NO costs for this 
 
- Once the tool has been used once, the workload can be viewed.
https://console.developers.google.com => Dashboard => Google Sheets API => Contingent
Divided into Read and Write Requests. A contingent of 500 per second is available free of charge (100 per user)


Therefore, by using more than one user, the workload can be increased. For this purpose, you can add further credentials in the project and adjust them in the configuration for the program (see 3 Configuration)
 
    
## 2. Create tables
- Next, the tables are created in which the data is to be uploaded. To do this go to https://drive.google.com.
- There you can add a new table (with right click).
- Give the table a name (top left), you will need this name later
- Add the previously created service account as editor
    - To do this, go to "**enable**" (top right) and add the e-mail of a service account as "**Editor**".
- Create more tables as desired
    
Hint:
 
One service account can be used for multiple tables.
You can also make a copy of [this](https://docs.google.com/spreadsheets/d/1tKFt1J4TWxT1GwSaB3TXyVDoWkIk3GAAeV6fNYKZvXk/edit?usp=sharing) table. This table already has a formatting that can be used. However, the name should still be adjusted. The service account must also be released.
 
## 3. configuration
In the folder "configurations" your configurations are stored, more can be added there (best use the template => Copy => Paste => Customize )
A text editor is required to edit them. The standard text editor of Windows is sufficient for this.
 
Which data the tool can display, which it can display approximately and which language is supported can be read [here](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/base_config_data.yml).
 
Which setting does what exactly can be read [here](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/config_file_structure), each setting itself can be changed. How the information has to be formatted can be seen in the templates.
 
### 3.1 Most important settings
The following are the most important settings to be made:
 
- "save_path" Path to the folder where the savegames are stored
- "save_game_name" Name of the save game to be read out
- "game_path" path to the folder where EU4 is installed
- "language" Language in which the data is uploaded
- "shown_info" Specifies which data should be shown in the table. The order in the list indicates the order in which they are in the table
- "approximation_info" data for which an approximate range is to be specified. (must also be present in "shown_info")
- "sheet_Information" for setting the individual tables  
    - the name of each table configuration must match a previously created table The data is uploaded to this table
    - "client_mail" E-mail of the service account enabled in the table 
    - "percent" Indicates how large the approximate range for this table should be
    - "continents" All countries with the capital in this continent are shown in this table
 
 
### 3.2 Other settings
- "dev_multi" Additional configuration for the "shown_info" "subject_dev Is a multiplier with which the development of the subjects is multiplied
- "tributary_include" Additional configuration for the "shown_info" "subject_dev". Specifies if tributary states should be included as well
 
- "is_trade_sheet" Indicates whether an additional spreadsheet with trading information should be added (percentage share for the trading bonus) 
 
- "standard_deviation" Indicates how much the approximate values change from update to update. Even if the data remains exactly the same, this value influences whether and how much the approximate range can change. At 0 nothing changes and the larger it is, the stronger the effect (10 recommended). You can find more information [here](https://gitlab.com/Nogrod/eu4-sheet/-/blob/master/Approximation.md).
 
 
## 4th Start
To use the program, the file "app.exe" must be executed. A window with a dialog will then open,
Select the desired configuration with its number and press Enter.
 
The table is updated initially at the start of the program, as well as when the selected savegame is changed (saved).
 
After that, all will run automatically. The window will then show information about the progress of the program, and
errors, if any.
